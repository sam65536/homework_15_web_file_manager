<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${path}</title>
</head>
<body>

    <form action="/file/create">
        <input type="hidden" name="path" value="${path}">
        <input type="text" name="name">
        <input type="submit" name="file" value="Create file" style="background-color: darkcyan;">
    </form>

    <c:if test="${parentPath != null}">
        <c:url value="/dir/view" var="viewURL">
            <c:param name="path" value="${parentPath}"/>
        </c:url>
        <a href="${viewURL}">Back</a>
        <br><br>
    </c:if>

    <c:forEach var="file" items="${files}">
        <form action="/file/remove">
            <input type="hidden" name="path" value="${file.key.toPath()}"/>
            <input type="submit" value="Delete" style="background-color: darkcyan;"/>
            <c:url value="${file.value}" var="viewURL">
                <c:param name="path" value="${file.key.toPath()}"/>
            </c:url>
            <a href="${viewURL}">${file.key.getName()}</a>
        </form>
    </c:forEach>

</body>
</html>
