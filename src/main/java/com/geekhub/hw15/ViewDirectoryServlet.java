package com.geekhub.hw15;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@WebServlet(value = "/dir/view", initParams = {
        @WebInitParam(name = "root", value = "C:\\")
})
public class ViewDirectoryServlet extends HttpServlet {

    private static Path ROOT_PATH;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ROOT_PATH = Paths.get(config.getInitParameter("root"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String stringPath = req.getParameter("path");
        Path path = stringPath == null ? ROOT_PATH : Paths.get(stringPath);
        req.setAttribute("path", path);
        req.setAttribute("parentPath", path.getParent());
        File[] files = new File(path.toString()).listFiles();
        String dirView = "/dir/view";
        String fileView = "/file/view";
        if (files != null) {
            Map<File, String> filesMap = new HashMap<>();
            Arrays.asList(files).forEach(file -> filesMap.put(file, file.isDirectory() ? dirView : fileView));
            req.setAttribute("files", filesMap);
        }
        req.getRequestDispatcher("/dirView.jsp").forward(req, resp);
    }
}
